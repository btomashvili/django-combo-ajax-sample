// reset element options
function clearComboElements(elements){
    for(var i = 0, len = elements.length; i < len; i++) {
        $(elements[i]+ " option").remove();
    }
}
// Get dependenc object from Dictionary get request
function getDependence(element,data) {
  $(element).append($('<option>', {value:'',text: ''})); // nullable value as first element
  for(var i = 0, len = data.length; i < len; i++) {

      $(element).append($('<option>', {
        value: data[i].id,
        text: data[i].name
      }));
  }
};

function getChildElements(element,id){

  $.get("/api/get-dict/"+ id , function(data) {
      getDependence(element,data);
    });
}
