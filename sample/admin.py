from django.contrib import admin
from models import Dictionary, Project
# Register your models here.

class DictionaryAdmin(admin.ModelAdmin):
    list_display = ('name','parrent')
    list_filter = ('name','parrent')

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name','shemadgneloba','zoma','erteulis_fasi')


admin.site.register(Dictionary,DictionaryAdmin)
admin.site.register(Project,ProjectAdmin)
