# -*- coding: utf-8 -*-
__author__ = 'Beka'
from models import Project, Dictionary
from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Button

class ProjectForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_class = 'form-horizontal'
        self.helper.add_input(Submit('submit', u'Save'))

    shemadgneloba = forms.ModelChoiceField(queryset=Dictionary.objects.filter(parrent__name=u'შემადგნელობა'))
    #zoma = forms.ModelChoiceField(queryset=Dictionary.objects.none(),required=False)
    zoma = forms.ChoiceField()
    erteulis_fasi = forms.ModelChoiceField(queryset=Dictionary.objects.none(),required=False)

    class Meta:
        model = Project
        fields = ('name', 'shemadgneloba', 'zoma','erteulis_fasi')
