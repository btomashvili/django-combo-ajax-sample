from django.db import models

# Create your models here.
class Dictionary(models.Model):
    name = models.CharField(max_length=200)
    parrent = models.ForeignKey('Dictionary',blank=True, null=True)
    create_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % (self.name)


class Project(models.Model):
    name = models.CharField(max_length=200)
    shemadgneloba = models.ForeignKey(Dictionary,blank=True, null=True,related_name='shemadgneloba')
    zoma = models.ForeignKey(Dictionary,blank=True, null=True,related_name='zoma')
    erteulis_fasi = models.ForeignKey(Dictionary,blank=True, null=True,related_name='erteulis_fasi')
    create_date = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return u'%s' % (self.name)
