# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sample', '0003_auto_20150713_1436'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('create_date', models.DateTimeField(auto_now=True)),
                ('erteulis_fasi', models.ForeignKey(related_name='erteulis_fasi', blank=True, to='sample.Dictionary', null=True)),
                ('shemadgneloba', models.ForeignKey(related_name='shemadgneloba', blank=True, to='sample.Dictionary', null=True)),
                ('zoma', models.ForeignKey(related_name='zoma', blank=True, to='sample.Dictionary', null=True)),
            ],
        ),
    ]
