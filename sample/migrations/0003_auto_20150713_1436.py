# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sample', '0002_auto_20150713_1435'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dictionary',
            name='create_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
