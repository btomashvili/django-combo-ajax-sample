from django.shortcuts import render, get_object_or_404 ,render_to_response,redirect,HttpResponse
from models import Dictionary, Project
from forms import ProjectForm
from django.template import RequestContext
import json

# Create your views here.
def index(request,project_id=None):
    p = get_object_or_404(Project, pk=project_id) if project_id else None
    form = ProjectForm(request.POST or None, instance=p)
    context = RequestContext(request)

    if request.method == 'POST': # save data
        ## ///TODO save data use forms  form.is_valid function
        ## also there is no error handling
        project = Project()
        project.name = request.POST['name']
        # because it's zoma and erteulis_fasi is instance of Dicationary Class
        project.shemadgneloba =  Dictionary.objects.get(id=request.POST['shemadgneloba'])
        project.zoma = Dictionary.objects.get(id=request.POST['zoma'])
        project.erteulis_fasi = Dictionary.objects.get(id=request.POST['erteulis_fasi'])
        project.save()
        return redirect("/")

    projects = Project.objects.order_by('-create_date')
    data = {
        'projects': projects,
        'project_id': project_id,
        'form' : form
    }
    return render_to_response("sample.html", data, context)

# REST API FOR Dictionary OBJECTS
# ///TODO Exception handling
def getdictionary(request,id=None):
    objs = (Dictionary.objects.filter(parrent=id).values('id', 'name'))
    json_objs = json.dumps(list(objs))
    return HttpResponse(json_objs,content_type="application/json")
